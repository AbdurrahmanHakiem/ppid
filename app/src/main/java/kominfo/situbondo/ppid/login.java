package kominfo.situbondo.ppid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import kominfo.situbondo.ppid.utils.ppidConfig;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

public class login extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextEmail=null;
    private EditText editTextPassword=null;
    private TextView title,regis;
    Button buttonLogin;
    private boolean loggedIn = false;
    String status,nik,id_skpd,token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        editTextEmail = (EditText) findViewById(R.id.nama);
        editTextPassword = (EditText) findViewById(R.id.pass);
        buttonLogin = (Button) findViewById(R.id.signin);
        buttonLogin.setOnClickListener(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(ppidConfig.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        loggedIn = sharedPreferences.getBoolean(ppidConfig.LOGGEDIN_SHARED_PREF, false);
        if(loggedIn){
            Intent intent = new Intent(login.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void login(){
//        Intent intent = new Intent(login.this, MainActivity.class);
//        startActivity(intent);
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ppid.situbondokab.go.id/api/login.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //If we are getting success from server
                        try {
                            JSONObject obj = new JSONObject(response);

                                SharedPreferences sharedPreferences = login.this.getSharedPreferences(ppidConfig.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                if(obj.getString("status").equals("01")) {
                                    Toast.makeText(login.this,"Maaf NIK atau Password yang dimasukan salah",Toast.LENGTH_LONG).show();
                                }else{
                                    nik = obj.getString("nik");
                                    id_skpd = obj.getString("id_skpd");
                                    token = obj.getString("token");

                                    editor.putBoolean(ppidConfig.LOGGEDIN_SHARED_PREF, true);
                                    editor.putString("nik", nik);
                                    editor.putString("id_skpd", id_skpd);
                                    editor.putString("token", token);

                                    editor.commit();

                                    Intent intent = new Intent(login.this, wizard.class);
                                    startActivity(intent);
                                }
                        }catch (JSONException e){

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding parameters to request
                params.put("username", email);
                params.put("password", password);

                //returning parameter
                return params;
            }
        };

        //Adding the string request to the queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        //Calling the login function
        login();
    }
}