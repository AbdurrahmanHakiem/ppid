package kominfo.situbondo.ppid;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.provider.OpenableColumns;
import android.os.Bundle;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import kominfo.situbondo.ppid.utils.ppidAdapter;
import kominfo.situbondo.ppid.utils.ppidConfig;
import kominfo.situbondo.ppid.utils.ppidRequest;
import kominfo.situbondo.ppid.utils.VolleyMultipartRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    Button SelectButton;
    FloatingActionButton UploadButton;
    EditText judul,keterangan ;
    Spinner kategori;
    String[] pilihan={"Berkala","Serta","Setiap"};
    String val = "";
    Uri uri;
    private ArrayList<HashMap<String, String>> arraylist;
    private RequestQueue rQueue;
    int nilai;
    public static final String PDF_UPLOAD_HTTP_URL = "http://ppid.situbondokab.go.id/api/upload.php";
    public int PDF_REQ_CODE = 1;
    String PdfNameHolder, PdfPathHolder, PdfID,keteranganx,nik,id_skpd,token;
    List<ppidRequest> DataAdapterClassList;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    JsonArrayRequest jsonArrayRequest ;
    ArrayList<String> SubjectNames;
    ArrayList<String> getnama;
    ArrayList<String> getkate;
    ArrayList<String> getgambar;
    ArrayList<String> getid_dip;
    String displayName;
    ArrayList<String> getsatuan;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<String> countries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DataAdapterClassList = new ArrayList<ppidRequest>();
        countries = new ArrayList<>();
        SubjectNames = new ArrayList<>();
        getkate = new ArrayList<>();
        getnama = new ArrayList<>();
        getgambar = new ArrayList<>();
        getsatuan = new ArrayList<>();
        getid_dip = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview1);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        recyclerView.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewlayoutManager);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        Tampilkan();

                                    }
                                }
        );

        AllowRunTimePermission();

        SelectButton = (Button) findViewById(R.id.addfile);
        UploadButton = (FloatingActionButton) findViewById(R.id.simpandata);
        judul = (EditText) findViewById(R.id.judulfile);
        keterangan = (EditText) findViewById(R.id.keterangan);
        kategori = (Spinner) findViewById(R.id.kategori);

        ArrayAdapter ab = new ArrayAdapter(this,android.R.layout.simple_spinner_item,pilihan);
        ab.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kategori.setAdapter(ab);

        kategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                val = pilihan[position];
                nilai = position +1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        SelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // PDF selection code start from here .

                Intent intent = new Intent();
                intent.setType("application/pdf");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PDF_REQ_CODE);
            }
        });

        UploadButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                uploadPDF(displayName,uri);
                judul.setText("");
                SelectButton.setText("Ketuk Disini untuk Memilih PDF");
                keterangan.setText("");
                }
        });


    }
    public void Tampilkan(){
        DataAdapterClassList.clear();
        swipeRefreshLayout.setRefreshing(true);
        jsonArrayRequest = new JsonArrayRequest(ppidConfig.URLLIST,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        UbahResponse(response);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }
    public void UbahResponse(JSONArray array){
        for(int i = 0; i<array.length(); i++){

            ppidRequest abc = new ppidRequest();

            JSONObject obj = null;
            try {
                obj = array.getJSONObject(i);
                if(obj != null){
                    String namaskpd = obj.getString(ppidConfig.NAMASKPD);
                    String judul = obj.getString(ppidConfig.JUDUL);
                    String kategori = obj.getString(ppidConfig.KATEGORI);
                    String keterangan = obj.getString(ppidConfig.KETERANGAN);
                    String file = obj.getString(ppidConfig.FILEPDF);
                    String id_dip = obj.getString(ppidConfig.ID_DIP);

                    abc.setNamaskpd(namaskpd);
                    abc.setJudul(judul);
                    abc.setKategori(kategori);
                    abc.setKeterangan(keterangan);
                    abc.setFile(file);
                    abc.setId_Dip(id_dip);

                    SubjectNames.add(obj.getString(ppidConfig.NAMASKPD));
                    getkate.add(obj.getString(ppidConfig.KATEGORI));
                    getnama.add(obj.getString(ppidConfig.JUDUL));
                    getgambar.add(obj.getString(ppidConfig.KETERANGAN));
                    getid_dip.add(obj.getString(ppidConfig.ID_DIP));
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            DataAdapterClassList.add(abc);
            countries.add(DataAdapterClassList.get(i).getNamaskpd());
        }
        runAnimation(recyclerView, 0);

    }
    private void runAnimation(RecyclerView recyclerView, int type){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller= null;

        if(type == 0)
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_slide_from_bottom);

        recyclerViewadapter = new ppidAdapter(DataAdapterClassList, this);
        recyclerView.setAdapter(recyclerViewadapter);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // Get the Uri of the selected file
            uri = data.getData();
            String uriString = uri.toString();
            File myFile = new File(uriString);
            String path = myFile.getAbsolutePath();
            displayName = null;

            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = this.getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        SelectButton.setText(data.getDataString());
                    }
                } finally {
                    cursor.close();

                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);

    }
    private void uploadPDF(final String pdfname, Uri pdffile){
        SharedPreferences sharedPreferences = getSharedPreferences(ppidConfig.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        id_skpd = sharedPreferences.getString(ppidConfig.ID_SKPD, "Not Available");
        token = sharedPreferences.getString(ppidConfig.TOKEN, "Not Available");
        nik = sharedPreferences.getString(ppidConfig.NIK, "Not Available");
        PdfNameHolder = judul.getText().toString().trim();
        keteranganx = keterangan.getText().toString().trim();
        if (uri == null) {
            Toast.makeText(this, "Tolong Pilih File PDF Terlebih Dahulu.", Toast.LENGTH_LONG).show();
        } else if (PdfNameHolder.isEmpty()) {
            Toast.makeText(this, "Isi Judul Terlebih Dahulu.", Toast.LENGTH_LONG).show();
        } else {
            InputStream iStream = null;
            try {
                iStream = getContentResolver().openInputStream(pdffile);
                final byte[] inputData = getBytes(iStream);
                Toast.makeText(getApplicationContext(), "Berkas Berhasil Di Upload", Toast.LENGTH_SHORT).show();
                VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, PDF_UPLOAD_HTTP_URL,
                        new Response.Listener<NetworkResponse>() {
                            @Override
                            public void onResponse(NetworkResponse response) {
                                rQueue.getCache().clear();
                                try {
                                    JSONObject jsonObject = new JSONObject(new String(response.data));
                                    jsonObject.toString().replace("\\\\", "");

                                    if (jsonObject.getString("status").equals("true")) {
                                        arraylist = new ArrayList<HashMap<String, String>>();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                   // Toast.makeText(getApplicationContext(), "error 1" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                               // Toast.makeText(getApplicationContext(), "error 2" + error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("username", nik);
                        params.put("token", token);
                        params.put("id_skpd", id_skpd);
                        params.put("judul", PdfNameHolder);
                        params.put("ket", keteranganx);
                        params.put("kategori", val);
                        return params;
                    }

                    @Override
                    protected Map<String, DataPart> getByteData() {
                        Map<String, DataPart> params = new HashMap<>();
                        params.put("upload", new DataPart(pdfname, inputData));

                        return params;
                    }
                };


                volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                rQueue = Volley.newRequestQueue(MainActivity.this);
                rQueue.add(volleyMultipartRequest);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public void AllowRunTimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE))
        {

            Toast.makeText(MainActivity.this,"READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(MainActivity.this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] Result) {

        switch (RC) {

            case 1:

                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(MainActivity.this,"Permission Granted", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(MainActivity.this,"Permission Canceled", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    @Override
    public void onRefresh() {
        Tampilkan();
    }
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(R.drawable.logoppid).setTitle("Keluar Aplikasi")
                .setMessage("Apakah anda akan keluar dari aplikasi ini ?")
                .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences preferences = getSharedPreferences(ppidConfig.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(ppidConfig.LOGGEDIN_SHARED_PREF, false);
                        editor.putString("nik", "");
                        editor.putString("id_skpd", "");
                        editor.putString("token", "");
                        editor.commit();
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                        finish();
                        deleteAppData();
                    }
                }).setNegativeButton("Tidak", null).show();
    }
    private void deleteAppData() {
        try {
            String packageName = getApplicationContext().getPackageName();
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear "+packageName);
            finish();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}