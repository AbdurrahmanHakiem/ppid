package kominfo.situbondo.ppid.utils;

public class ppidRequest {
    String namaskpd;
    String judul;
    String kategori;
    String keterangan;
    String file;
    String id_dip;


    public String getNamaskpd() {

        return namaskpd;
    }

    public void setNamaskpd(String namaskpd) {

        this.namaskpd = namaskpd;
    }

    public String getJudul() {

        return judul;
    }

    public void setJudul(String judul) {

        this.judul = judul;
    }

    public String getKategori() {

        return kategori;
    }

    public void setKategori(String kategori) {

        this.kategori = kategori;
    }

    public String getKeterangan() {

        return keterangan;
    }

    public void setKeterangan(String keterangan) {

        this.keterangan = keterangan;
    }

    public String getFile() {

        return file;
    }

    public void setFile(String file) {

        this.file = file;
    }

    public String getId_Dip() {

        return id_dip;
    }

    public void setId_Dip(String id_dip) {

        this.id_dip = id_dip;
    }

}
