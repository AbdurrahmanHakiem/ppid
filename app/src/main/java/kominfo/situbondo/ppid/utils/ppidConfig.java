package kominfo.situbondo.ppid.utils;

public class ppidConfig {

    public static final String URLLIST = "http://ppid.situbondokab.go.id/api/list_dip.php";
    public static final String NAMASKPD = "nama_skpd";
    public static final String JUDUL = "judul";
    public static final String KATEGORI = "kategori";
    public static final String KETERANGAN = "keterangan";
    public static final String FILEPDF = "file_pdf";
    public static final String ID_DIP = "id_dip";

    public static final String NIK = "nik";
    public static final String ID_SKPD = "id_skpd";
    public static final String TOKEN = "token";

    public static final String URLTOTAL = "http://ppid.situbondokab.go.id/api/total_layanan.php";
    public static final String TOTALJML = "total_Jumlah_informasi_publik";
    public static final String TOTALPEMOHON = "total_Jumlah_pemohon";
    public static final String TOTALKEBERATAN = "total_keberatan";

    public static final String LOGIN_SUCCESS = "00";
    public static final String SHARED_PREF_NAME = "myloginapp";
    public static final String LOGGEDIN_SHARED_PREF = "loggedin";
}
