package kominfo.situbondo.ppid.utils;

public class ppidDetailRequest {
    String informasi;
    String permohonan;
    String keberatan;


    public String getInformasi() {

        return informasi;
    }

    public void setInformasi(String informasi) {

        this.informasi = informasi;
    }

    public String getPermohonan() {

        return permohonan;
    }

    public void setPermohonan(String permohonan) {

        this.permohonan = permohonan;
    }

    public String getKeberatan() {

        return keberatan;
    }

    public void setKeberatan(String keberatan) {

        this.keberatan = keberatan;
    }

}
