package kominfo.situbondo.ppid.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import kominfo.situbondo.ppid.R;


public class ppidAdapter extends RecyclerView.Adapter<ppidAdapter.ViewHolder> {

    Context context;
    List<ppidRequest> dataAdapters;

    public ppidAdapter(List<ppidRequest> getDataAdapter, Context context){

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ppidAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ui_ap_ppid, parent, false);
        return new ppidAdapter.ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(ppidAdapter.ViewHolder viewHolder, int position) {

        final ppidRequest dataAdapter =  dataAdapters.get(position);
            try {
                viewHolder.namaskpd.setText(dataAdapter.getNamaskpd());
                viewHolder.judul.setText(dataAdapter.getJudul());
                viewHolder.kategori.setText(dataAdapter.getKategori());
                viewHolder.keterangan.setText(dataAdapter.getKeterangan());
                viewHolder.pencet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataAdapter.getFile()));
                        context.startActivity(browserIntent);
//                       Toast.makeText(this,dataAdapter.getFile(),Toast.LENGTH_LONG).show();
                    }
                });
                viewHolder.hapus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(context).setIcon(R.drawable.ic_baseline_delete_forever_24).setTitle("Hapus Rekaman")
                                .setMessage("Apakah anda Ingin Menghapus Rekaman ini?")
                                .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ppid.situbondokab.go.id/api/hapus.php",
                                                new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {
                                                        //If we are getting success from server
                                                        try {
                                                            JSONObject obj = new JSONObject(response);
                                                            String status = obj.getString("status_code");
                                                            if (status.equalsIgnoreCase(ppidConfig.LOGIN_SUCCESS)) {
                                                                Toast.makeText(context, "Berhasil dihapus", Toast.LENGTH_LONG).show();
                                                            } else {
                                                                Toast.makeText(context, "Gagal dihapus", Toast.LENGTH_LONG).show();

                                                            }
                                                        } catch (JSONException e) {

                                                        }

                                                    }
                                                },
                                                new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        Toast.makeText(context, "Berhasil dihapus 4"+error.getMessage(), Toast.LENGTH_LONG).show();
                                                    }
                                                }) {
                                            @Override
                                            protected Map<String, String> getParams() {
                                                Map<String, String> params = new HashMap<>();
                                                //Adding parameters to request
                                                params.put("id", dataAdapter.getId_Dip());

                                                //returning parameter
                                                return params;
                                            }
                                        };
                                        RequestQueue requestQueue = Volley.newRequestQueue(context);
                                        requestQueue.add(stringRequest);
                                    }
                                }).setNegativeButton("Tidak", null).show();
                    }
                });
            }catch (NullPointerException e){

            }catch (NumberFormatException e){

            }
    }
    @Override
    public int getItemCount() {
        return (dataAdapters!=null? dataAdapters.size():0);
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView namaskpd;
        public TextView judul;
        public TextView kategori;
        public TextView keterangan;
        public CardView pencet;
        public ImageButton hapus;

        public ViewHolder(View itemView, int viewType) {

            super(itemView);

            namaskpd = itemView.findViewById(R.id.namaskpd);
            judul = itemView.findViewById(R.id.judul);
            kategori = itemView.findViewById(R.id.kategori);
            keterangan = itemView.findViewById(R.id.ket);
            pencet = itemView.findViewById(R.id.pencet);
            hapus = itemView.findViewById(R.id.hapus);

        }
    }
}
