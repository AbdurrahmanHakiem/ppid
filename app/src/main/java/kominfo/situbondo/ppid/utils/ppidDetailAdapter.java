package kominfo.situbondo.ppid.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kominfo.situbondo.ppid.R;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ppidDetailAdapter extends RecyclerView.Adapter<ppidDetailAdapter.ViewHolder> {

    Context context;
    List<ppidDetailRequest> dataAdapters;

    public ppidDetailAdapter(List<ppidDetailRequest> getDataAdapter, Context context){

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ppidDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ui_ap_ppidtotal, parent, false);
        return new ppidDetailAdapter.ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(ppidDetailAdapter.ViewHolder viewHolder, int position) {

        final ppidDetailRequest dataAdapter =  dataAdapters.get(position);
            try {
                viewHolder.informasi.setText(dataAdapter.getInformasi());
                viewHolder.pemohon.setText(dataAdapter.getPermohonan());
                viewHolder.keberatan.setText(dataAdapter.getKeberatan());
            }catch (NullPointerException e){

            }catch (NumberFormatException e){

            }
    }
    @Override
    public int getItemCount() {
        return (dataAdapters!=null? dataAdapters.size():0);
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView informasi;
        public TextView pemohon;
        public TextView keberatan;

        public ViewHolder(View itemView, int viewType) {

            super(itemView);

            informasi = itemView.findViewById(R.id.informasi);
            pemohon = itemView.findViewById(R.id.pemohon);
            keberatan = itemView.findViewById(R.id.keberatan);

        }
    }
}
